﻿using Autofac;
using Xapo.Infrastructure.Di;

namespace Xapo.Services.Infrastructure.Configuration.Di
{
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder) =>
            builder.RegisterByNamespacePart(
                typeof(ServicesModule).Assembly, 
                "Infrastructure.Repository");
    }
}
