﻿using Microsoft.Extensions.Logging;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Xapo.Infrastructure.Storage;
using Xapo.Services.Infrastructure.Configuration;
using Xapo.Services.Model.Entities;

namespace Xapo.Services.Infrastructure.Storage
{
    public static class CollectionManagement
    {
        public static void BuildCollections(IMongoDbContext ctx, ILogger logger)
        {
            CreateCollections(ctx);
            MapCollections();
            CreateWallet(ctx, logger);
        }

        // it's necessary to create collections upfront, because when N collections
        // operations are a part of transaction, mongodb expects them to already exist
        private static void CreateCollections(IMongoDbContext ctx)
        {
            ctx.GetOrCreateCollection<Wallet>();
            ctx.GetOrCreateCollection<Order>();
            ctx.GetOrCreateCollection<ExchangeRate>();
        }

        private static void MapCollections()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Wallet)))
                BsonClassMap.RegisterClassMap<Wallet>(cm =>
                {
                    cm.AutoMap();
                    cm.SetIdMember(cm.GetMemberMap(w => w.Id));
                });

            if (!BsonClassMap.IsClassMapRegistered(typeof(Order)))
                BsonClassMap.RegisterClassMap<Order>(cm =>
                {
                    cm.AutoMap();
                    cm.SetIdMember(cm.GetMemberMap(o => o.Id));
                });

            if (!BsonClassMap.IsClassMapRegistered(typeof(ExchangeRate)))
                BsonClassMap.RegisterClassMap<ExchangeRate>(cm =>
                {
                    cm.AutoMap();
                    cm.SetIdMember(cm.GetMemberMap(r => r.Id));
                });
        }

        private static void CreateWallet(IMongoDbContext ctx, ILogger logger)
        {
            try
            {
                ctx
                    .GetOrCreateCollection<Wallet>()
                    .InsertOne(new Wallet(Constants.WalletId));
            }
            catch (MongoWriteException exc)
            {
                // one situation it could happen in is when a wallet with the given
                // id already exists
                // in that case it's safe to ignore the exception
                // logging it here just in case...
                logger.LogInformation(exc, "An exception has occurred when initializing a wallet object.");
            }
        }
    }
}
