﻿using MongoDB.Driver;
using System.Threading.Tasks;
using Xapo.Infrastructure.Storage;
using Xapo.Services.Model.Entities;
using Xapo.Services.Model.Enums;

namespace Xapo.Services.Infrastructure.Repository
{
    public class ExchangeRateRepository : IExchangeRateRepository
    {
        private readonly IMongoDbContext _dbContext;

        public ExchangeRateRepository(IMongoDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task Upsert(ExchangeRate rate) =>
            await _dbContext
                .GetOrCreateCollection<ExchangeRate>()
                .ReplaceOneAsync(
                    r => r.Id == rate.Id,
                    rate,
                    new ReplaceOptions {IsUpsert = true});

        public async Task<ExchangeRate> Load(CurrencyCode code) =>
            await _dbContext
                .GetOrCreateCollection<ExchangeRate>()
                .Find(r => r.Id == code)
                .FirstOrDefaultAsync();
    }
}
