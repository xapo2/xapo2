﻿using MongoDB.Driver;
using System.Threading.Tasks;
using Xapo.Infrastructure.Storage;
using Xapo.Services.Infrastructure.Configuration;
using Xapo.Services.Model.Entities;

namespace Xapo.Services.Infrastructure.Repository
{
    public class WalletRepository : IWalletRepository
    {
        private readonly IMongoDbContext _dbContext;

        public WalletRepository(IMongoDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task Update(Wallet wallet)
        {
            //var transaction = _dbContext.JoinTransaction();

            await _dbContext
                .GetOrCreateCollection<Wallet>()
                .ReplaceOneAsync(
                    //transaction,
                    Builders<Wallet>.Filter.Eq(b => b.Id, wallet.Id) &
                    Builders<Wallet>.Filter.Eq(b => b.Version, wallet.Version - 1),
                    wallet);
        }

        public async Task<Wallet> Load() =>
            await _dbContext
                .GetOrCreateCollection<Wallet>()
                .Find(FilterDefinition<Wallet>.Empty)
                .FirstOrDefaultAsync();

        public async Task<Wallet> Load(int token) =>
            await _dbContext
                .GetOrCreateCollection<Wallet>()
                .Find(b => b.Id == Constants.WalletId && b.Version == token)
                .FirstOrDefaultAsync();
    }
}
