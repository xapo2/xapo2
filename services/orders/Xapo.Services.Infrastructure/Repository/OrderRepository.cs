﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xapo.Infrastructure.Storage;
using Xapo.Services.Model.Entities;

namespace Xapo.Services.Infrastructure.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly IMongoDbContext _dbContext;

        public OrderRepository(IMongoDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task Add(Order order) =>
            await _dbContext
                .GetOrCreateCollection<Order>()
                .InsertOneAsync(order);

        public async Task<IEnumerable<Order>> Load(int page, int pageSize) =>
            await _dbContext
                .GetOrCreateCollection<Order>()
                .Find(FilterDefinition<Order>.Empty)
                .SortByDescending(o => o.CreatedAt)
                .Skip(page * pageSize)
                .Limit(pageSize)
                .ToListAsync();

        public async Task<long> Count() =>
            await _dbContext
                .GetOrCreateCollection<Order>()
                .CountDocumentsAsync(FilterDefinition<Order>.Empty);
    }
}
