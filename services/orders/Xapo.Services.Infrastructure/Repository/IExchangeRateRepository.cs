﻿using System.Threading.Tasks;
using Xapo.Services.Model.Entities;
using Xapo.Services.Model.Enums;

namespace Xapo.Services.Infrastructure.Repository
{
    public interface IExchangeRateRepository
    {
        Task Upsert(ExchangeRate rate);

        Task<ExchangeRate> Load(CurrencyCode code);
    }
}
