﻿using System.Threading.Tasks;
using Xapo.Services.Model.Entities;

namespace Xapo.Services.Infrastructure.Repository
{
    public interface IWalletRepository
    {
        Task Update(Wallet wallet);

        Task<Wallet> Load();

        Task<Wallet> Load(int token);
    }
}
