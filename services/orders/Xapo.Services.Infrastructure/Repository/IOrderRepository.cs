﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xapo.Services.Model.Entities;

namespace Xapo.Services.Infrastructure.Repository
{
    public interface IOrderRepository
    {
        Task Add(Order order);

        Task<IEnumerable<Order>> Load(int page, int pageSize);

        Task<long> Count();
    }
}
