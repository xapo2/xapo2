﻿using Flurl.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xapo.Infrastructure.Configuration;
using Xapo.Services.Model.Entities;
using Xapo.Services.Model.Enums;

namespace Xapo.Services.Application.Services.ExchangeRateStrategies
{
    public class CoindeskApiStrategy : IExchangeRateApiStrategy
    {
        private readonly IRemoteApiClientFactory _apiClientFactory;
        private readonly IOptions<ExchangeRatesApis> _options;

        public CoindeskApiStrategy(
            IRemoteApiClientFactory apiClientFactory, 
            IOptions<ExchangeRatesApis> options)
        {
            _apiClientFactory = apiClientFactory;
            _options = options;
        }

        public bool CanRun() =>
            _options
                .Value
                .ApiOptions
                ?.Any(o => o.Key == nameof(CoindeskApiStrategy) && o.Value.IsEnabled) == true;

        public async Task<ExchangeRate> GetRate(CurrencyCode code)
        {
            var config = _options
                .Value
                .ApiOptions
                .First(o => o.Key == nameof(CoindeskApiStrategy))
                .Value;
            var address = config.Address;
            var response = await _apiClientFactory.Get(address)
                .Request()
                .GetAsync()
                .ReceiveJson<CoindeskResponse>();
            var apiRate = code switch
            {
                CurrencyCode.Eur => response.Bpi.Eur,
                CurrencyCode.Gbp => response.Bpi.Gbp,
                CurrencyCode.Usd => response.Bpi.Usd
            };

            return new ExchangeRate(
                code, 
                DateTime.Parse(response.Time.Updated), 
                Convert.ToDecimal(apiRate.RateFloat));
        }

        private class CoindeskResponse
        {
            public Time Time { get; set; }

            public Bpi Bpi { get; set; }
        }

        private class Time
        {
            [JsonProperty("updatedISO")]
            public string Updated { get; set; }
        }

        private class Bpi
        {
            [JsonProperty("USD")]
            public ApiRate Usd { get; set; }

            [JsonProperty("GBP")]
            public ApiRate Gbp { get; set; }

            [JsonProperty("EUR")]
            public ApiRate Eur { get; set; }
        }

        private class ApiRate
        {
            public string Code { get; set; }

            [JsonProperty("rate_float")]
            public float RateFloat { get; set; }
        }
    }
}
