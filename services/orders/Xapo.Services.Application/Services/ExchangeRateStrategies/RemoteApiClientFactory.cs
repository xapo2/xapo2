﻿using Flurl;
using Flurl.Http;
using Flurl.Http.Configuration;

namespace Xapo.Services.Application.Services.ExchangeRateStrategies
{
    public class RemoteApiClientFactory : PerBaseUrlFlurlClientFactory, IRemoteApiClientFactory
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public RemoteApiClientFactory(IHttpClientFactory httpClientFactory) =>
            _httpClientFactory = httpClientFactory;

        protected override IFlurlClient Create(Url url) =>
            base
                .Create(url)
                .Configure(settings => settings.HttpClientFactory = _httpClientFactory);
    }
}
