﻿using Flurl.Http.Configuration;

namespace Xapo.Services.Application.Services.ExchangeRateStrategies
{
    public interface IRemoteApiClientFactory : IFlurlClientFactory
    {
    }
}
