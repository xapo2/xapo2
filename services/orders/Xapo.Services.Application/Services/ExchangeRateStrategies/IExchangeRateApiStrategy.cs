﻿using System.Threading.Tasks;
using Xapo.Services.Model.Entities;
using Xapo.Services.Model.Enums;

namespace Xapo.Services.Application.Services.ExchangeRateStrategies
{
    public interface IExchangeRateApiStrategy
    {
        bool CanRun();

        Task<ExchangeRate> GetRate(CurrencyCode code);
    }
}
