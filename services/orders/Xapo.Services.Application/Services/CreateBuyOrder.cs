﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xapo.Infrastructure.Storage;
using Xapo.Services.Application.Dtos.Requests;
using Xapo.Services.Application.Dtos.Results.CreateBuyOrder;
using Xapo.Services.Application.Services.ExchangeRateStrategies;
using Xapo.Services.Infrastructure.Repository;
using Xapo.Services.Model.Entities;
using Xapo.Services.Model.Enums;

namespace Xapo.Services.Application.Services
{
    public class CreateBuyOrder
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWalletRepository _walletRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IExchangeRateRepository _exchangeRateRepository;
        private readonly IEnumerable<IExchangeRateApiStrategy> _exchangeRateLoadingStrategies;
        private readonly ILogger<CreateBuyOrder> _logger;

        public CreateBuyOrder(
            IUnitOfWork unitOfWork,
            IWalletRepository walletRepository,
            IOrderRepository orderRepository,
            IExchangeRateRepository exchangeRateRepository,
            IEnumerable<IExchangeRateApiStrategy> exchangeRateLoadingStrategies,
            ILogger<CreateBuyOrder> logger)
        {
            _unitOfWork = unitOfWork;
            _walletRepository = walletRepository;
            _orderRepository = orderRepository;
            _exchangeRateRepository = exchangeRateRepository;
            _exchangeRateLoadingStrategies = exchangeRateLoadingStrategies;
            _logger = logger;
        }

        public async Task<ICreateBuyOrderResult> Execute(CreateBuyOrderCommand command)
        {
            if (command.Amount <= 0)
                return new CreateBuyOrderErrorResult(CreateBuyOrderErrorCode.AmountNegativeOrZero);

            var exchangeRate = (await GetOrCreateRate(command.CurrencyCode))?.Rate;

            if (exchangeRate is null)
                return new CreateBuyOrderErrorResult(CreateBuyOrderErrorCode.NoExchangeRateAvailable);

            var wallet = await _walletRepository.Load(command.WalletToken);

            if (wallet is null)
                return new CreateBuyOrderErrorResult(CreateBuyOrderErrorCode.WalletVersionMismatch);

            var btcAmount = decimal.Round(command.Amount / exchangeRate.Value, 8);
            var walletValueAfterTransaction = wallet.AvailableAmount - btcAmount;

            if (walletValueAfterTransaction < 0)
                return new CreateBuyOrderErrorResult(CreateBuyOrderErrorCode.AmountExceedsAvailableAmount);

            return await CreateOrder(command, exchangeRate.Value, btcAmount, wallet);
        }

        private async Task<ExchangeRate> GetOrCreateRate(CurrencyCode code)
        {
            var now = DateTime.UtcNow;
            var storedRate = await _exchangeRateRepository.Load(code);

            if (ShouldLoadNewExchangeRate(storedRate, now))
            {
                var strategy = _exchangeRateLoadingStrategies.FirstOrDefault(s => s.CanRun());

                if (strategy is null)
                    return null;

                storedRate = await strategy.GetRate(code);

                try
                {
                    await _exchangeRateRepository.Upsert(storedRate);
                }
                catch (Exception exc)
                {
                    _logger.LogWarning(exc, "Another instance of the app has already upserted this exchange rate.");

                    storedRate = await _exchangeRateRepository.Load(code);
                }
            }

            return storedRate;
        }

        private async Task<ICreateBuyOrderResult> CreateOrder(CreateBuyOrderCommand command, decimal exchangeRate, decimal btcAmount, Wallet wallet)
        {
            //using var transaction = await _unitOfWork.CreateTransaction();

            try
            {
                var order = new Order(command.Amount, command.CurrencyCode, exchangeRate, btcAmount);

                wallet.DecreaseAvailableAmount(btcAmount);

                await _walletRepository.Update(wallet);
                await _orderRepository.Add(order);
                //await _unitOfWork.Commit();
            }
            catch (Exception exc)
            {
                //await _unitOfWork.Rollback();

                _logger.LogError(exc, "Something went wrong while creating a new order");

                // more often than an actual database failure, user transaction conflict
                // due to wallet version mismatch will happen
                // that's why we should always log the exception, but tell the client about
                // the actually more probable error, so that they can retry
                return new CreateBuyOrderErrorResult(CreateBuyOrderErrorCode.WalletVersionMismatch);
            }

            return new CreateBuyOrderOkResult();
        }

        private static bool ShouldLoadNewExchangeRate(ExchangeRate storedRate, DateTime now) =>
            storedRate is null || Math.Ceiling((now - storedRate.CreatedAt).TotalSeconds) >= 60;
    }
}
