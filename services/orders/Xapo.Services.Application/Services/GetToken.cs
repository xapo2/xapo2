﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Xapo.Services.Application.Dtos.Results.GetToken;
using Xapo.Services.Infrastructure.Repository;

namespace Xapo.Services.Application.Services
{
    public class GetToken
    {
        private readonly IWalletRepository _walletRepository;
        private readonly ILogger<GetToken> _logger;

        public GetToken(
            IWalletRepository walletRepository,
            ILogger<GetToken> logger)
        {
            _walletRepository = walletRepository;
            _logger = logger;
        }

        public async Task<IGetTokenResult> Execute()
        {
            try
            {
                var wallet = await _walletRepository.Load();

                if (wallet is null)
                    return new GetTokenErrorResult(GetTokenErrorCode.WalletNotFound);

                return new GetTokenOkResult(wallet.Version);
            }
            catch (Exception exc)
            {
                _logger.LogError(exc, "An error has occurred while loading a wallet.");

                return new GetTokenErrorResult(GetTokenErrorCode.InternalError);
            }
        }
    }
}
