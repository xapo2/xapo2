﻿using System.Linq;
using System.Threading.Tasks;
using Xapo.Services.Application.Dtos.Results.GetOrders;
using Xapo.Services.Infrastructure.Repository;

namespace Xapo.Services.Application.Services
{
    public class GetOrders
    {
        private const int PageSize = 10;
        private readonly IOrderRepository _orderRepository;

        public GetOrders(IOrderRepository orderRepository) =>
            _orderRepository = orderRepository;

        public async Task<GetOrdersResult> Execute(int page)
        {
            var orders = await _orderRepository.Load(page, PageSize);
            var count = await _orderRepository.Count();
            var result = new GetOrdersResult(
                new Paging(page, count),
                orders.Select(o => new Order(
                    o.Id,
                    o.CreatedAt,
                    o.FiatAmount,
                    o.Currency,
                    o.ExchangeRate,
                    o.BitcoinAmount)));

            return result;
        }
    }
}
