﻿using Autofac;
using Xapo.Infrastructure.Di;
using Xapo.Infrastructure.Storage;
using Module = Autofac.Module;

namespace Xapo.Services.Application.Configuration.Di
{
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var ns = "Application.Services";

            builder
                .RegisterAssemblyTypes(typeof(ServicesModule).Assembly)
                .Where(t =>
                    !string.IsNullOrEmpty(t.Namespace) &&
                    t.Namespace.Contains(ns))
                .AsSelf()
                .InstancePerLifetimeScope()
                .PreserveExistingDefaults();
            builder.RegisterByNamespacePart(typeof(ServicesModule).Assembly, ns);
            builder
                .RegisterType<MongoDbContext>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
