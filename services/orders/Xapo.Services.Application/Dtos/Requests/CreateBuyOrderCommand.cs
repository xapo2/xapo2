﻿using Xapo.Services.Model.Enums;

namespace Xapo.Services.Application.Dtos.Requests
{
    public class CreateBuyOrderCommand
    {
        public int WalletToken { get; set; }

        public decimal Amount { get; set; }

        public CurrencyCode CurrencyCode { get; set; }
    }
}
