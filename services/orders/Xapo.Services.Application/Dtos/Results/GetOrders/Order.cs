﻿using System;
using Xapo.Services.Model.Enums;

namespace Xapo.Services.Application.Dtos.Results.GetOrders
{
    public class Order
    {
        public Guid Id { get; }

        public DateTime CreatedAt { get; }

        public decimal FiatAmount { get; }

        public CurrencyCode Currency { get; }

        public decimal ExchangeRate { get; }

        public decimal BitcoinAmount { get; }

        public Order(Guid id, DateTime createdAt, decimal fiatAmount, CurrencyCode currency, decimal exchangeRate, decimal bitcoinAmount)
        {
            Id = id;
            CreatedAt = createdAt;
            FiatAmount = fiatAmount;
            Currency = currency;
            ExchangeRate = exchangeRate;
            BitcoinAmount = bitcoinAmount;
        }
    }
}
