﻿namespace Xapo.Services.Application.Dtos.Results.GetOrders
{
    public class Paging
    {
        public int Page { get; }

        public long Total { get; }

        public Paging(int page, long total)
        {
            Page = page;
            Total = total;
        }
    }
}
