﻿using System.Collections.Generic;

namespace Xapo.Services.Application.Dtos.Results.GetOrders
{
    public class GetOrdersResult
    {
        public Paging Paging { get; }

        public IEnumerable<Order> Orders { get; }

        public GetOrdersResult(Paging paging, IEnumerable<Order> orders)
        {
            Paging = paging;
            Orders = orders;
        }
    }
}
