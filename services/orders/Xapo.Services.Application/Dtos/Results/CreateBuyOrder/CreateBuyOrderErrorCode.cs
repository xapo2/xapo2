﻿namespace Xapo.Services.Application.Dtos.Results.CreateBuyOrder
{
    public enum CreateBuyOrderErrorCode
    {
        AmountNegativeOrZero,
        AmountExceedsAvailableAmount,
        NoExchangeRateAvailable,
        WalletVersionMismatch
    }
}