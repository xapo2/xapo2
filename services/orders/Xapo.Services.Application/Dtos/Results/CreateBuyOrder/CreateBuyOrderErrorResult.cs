﻿namespace Xapo.Services.Application.Dtos.Results.CreateBuyOrder
{
    public class CreateBuyOrderErrorResult : ICreateBuyOrderResult
    {
        public CreateBuyOrderErrorCode ErrorCode { get; }

        public CreateBuyOrderErrorResult(CreateBuyOrderErrorCode errorCode) =>
            ErrorCode = errorCode;
    }
}
