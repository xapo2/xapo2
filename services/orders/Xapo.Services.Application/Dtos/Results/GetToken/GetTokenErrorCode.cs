﻿namespace Xapo.Services.Application.Dtos.Results.GetToken
{
    public enum GetTokenErrorCode
    {
        WalletNotFound,
        InternalError
    }
}
