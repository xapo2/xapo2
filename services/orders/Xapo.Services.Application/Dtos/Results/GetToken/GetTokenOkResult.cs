﻿namespace Xapo.Services.Application.Dtos.Results.GetToken
{
    public class GetTokenOkResult : IGetTokenResult
    {
        public int Token { get; }

        public GetTokenOkResult(int token) =>
            Token = token;
    }
}
