﻿namespace Xapo.Services.Application.Dtos.Results.GetToken
{
    public class GetTokenErrorResult : IGetTokenResult
    {
        public GetTokenErrorCode ErrorCode { get; }

        public GetTokenErrorResult(GetTokenErrorCode errorCode) =>
            ErrorCode = errorCode;
    }
}
