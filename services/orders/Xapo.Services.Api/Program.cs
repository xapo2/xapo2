using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog.Core;
using System;
using Xapo.Infrastructure.Logging;

namespace Xapo.Services.Api
{
    public class Program
    {
        public static LoggingLevelSwitch LogSwitch = new LoggingLevelSwitch();

        public static void Main(string[] args) =>
            CreateHostBuilder(args).Build().Run();

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host
                .CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webHost =>
                    webHost
                        .UseShutdownTimeout(TimeSpan.FromSeconds(10))
                        .SuppressStatusMessages(false)
                        .UseLogging(LogSwitch)
                        .UseStartup<Startup>()
                        .UseAppConfig(typeof(Startup)));
    }
}