using Autofac;
using Flurl.Http.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization.Conventions;
using Newtonsoft.Json.Converters;
using Xapo.Infrastructure.Api;
using Xapo.Infrastructure.Configuration;
using Xapo.Infrastructure.Remoting;
using Xapo.Infrastructure.Storage;
using Xapo.Services.Infrastructure.Storage;
using ApplicationServicesModule = Xapo.Services.Application.Configuration.Di.ServicesModule;
using InfrastructureServicesModule = Xapo.Services.Infrastructure.Configuration.Di.ServicesModule;

namespace Xapo.Services.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                });
            services.Configure<ApiBehaviorOptions>(options => options.SuppressModelStateInvalidFilter = true)
                .AddSwaggerDocs(Configuration, typeof(Startup).Namespace);
            services.AddSingleton<IHttpClientFactory, BasicHttpClientFactory>();
            services
                .Configure<SwaggerOptions>(Configuration.GetSection(nameof(SwaggerOptions)))
                .Configure<MongoDbOptions>(Configuration.GetSection(nameof(MongoDbOptions)))
                .Configure<ExchangeRatesApis>(Configuration.GetSection(nameof(ExchangeRatesApis)));
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new ApplicationServicesModule());
            builder.RegisterModule(new InfrastructureServicesModule());
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            ConfigureDatabase(app, logger);
            app
                .UseSwaggerDocs()
                .UseRouting()
                .UseCors(options => options.AllowAnyOrigin().AllowAnyMethod())
                .UseEndpoints(endpoints => endpoints.MapControllers());
            SetMongoConventions();
        }

        private void ConfigureDatabase(IApplicationBuilder app, ILogger<Startup> logger)
        {
            var options = app.ApplicationServices.GetRequiredService<IOptions<MongoDbOptions>>();

            CollectionManagement.BuildCollections(new MongoDbContext(options), logger);
        }

        private static void SetMongoConventions() =>
            ConventionRegistry.Register(
                nameof(ImmutableObjectConvention),
                new ConventionPack { new ImmutableObjectConvention() },
                _ => true);
    }
}
