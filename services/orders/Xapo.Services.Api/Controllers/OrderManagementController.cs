﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Xapo.Services.Application.Dtos.Requests;
using Xapo.Services.Application.Dtos.Results.CreateBuyOrder;
using Xapo.Services.Application.Dtos.Results.GetOrders;
using Xapo.Services.Application.Dtos.Results.GetToken;
using Xapo.Services.Application.Services;

namespace Xapo.Services.Api.Controllers
{
    [ApiController]
    [Route("api/orders")]
    public class OrderManagementController : ControllerBase
    {
        private readonly ILogger<OrderManagementController> _logger;
        private readonly CreateBuyOrder _createBuyOrder;
        private readonly GetToken _getToken;
        private readonly GetOrders _getOrders;

        public OrderManagementController(
            ILogger<OrderManagementController> logger,
            CreateBuyOrder createBuyOrder,
            GetToken getToken, 
            GetOrders getOrders)
        {
            _logger = logger;
            _createBuyOrder = createBuyOrder;
            _getToken = getToken;
            _getOrders = getOrders;
        }

        [HttpPost]
        [ProducesResponseType(typeof(CreateBuyOrderOkResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(CreateBuyOrderErrorResult), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ICreateBuyOrderResult>> Post(CreateBuyOrderCommand command)
        {
            var result = await _createBuyOrder.Execute(command);

            return result switch
            {
                CreateBuyOrderOkResult r => Ok(r),
                CreateBuyOrderErrorResult r => BadRequest(r),
                object x
                    when x.GetType() != typeof(CreateBuyOrderOkResult) &&
                         x.GetType() != typeof(CreateBuyOrderErrorResult) => UnknownResult(x, nameof(CreateBuyOrder))
            };
        }

        [HttpGet("token")]
        [ProducesResponseType(typeof(GetTokenOkResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(GetTokenErrorResult), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<GetTokenOkResult>> Get()
        {
            var result = await _getToken.Execute();

            return result switch
            {
                GetTokenOkResult r => Ok(r),
                GetTokenErrorResult r => BadRequest(r),
                object x
                    when x.GetType() != typeof(GetTokenOkResult) &&
                         x.GetType() != typeof(GetTokenErrorResult) => UnknownResult(x, nameof(CreateBuyOrder))
            };
        }

        [HttpGet("{page}")]
        [ProducesResponseType(typeof(GetOrdersResult), StatusCodes.Status200OK)]
        public async Task<ActionResult<GetOrdersResult>> Get(int page)
        {
            var result = await _getOrders.Execute(page);

            return Ok(result);
        }

        private ActionResult UnknownResult(object x, string svcName)
        {
            _logger.LogError(
                "The {Service} service has returned an unknown result that cannot be handled. Contents are: {Contents}",
                svcName,
                JsonConvert.SerializeObject(x));

            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
