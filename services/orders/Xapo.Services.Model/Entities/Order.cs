﻿using System;
using Xapo.Services.Model.Enums;

namespace Xapo.Services.Model.Entities
{
    public class Order
    {
        public Guid Id { get; private set; }
        
        public DateTime CreatedAt { get; private set; }

        public decimal FiatAmount { get; private set; }

        public CurrencyCode Currency { get; private set; }

        public decimal ExchangeRate { get; private set; }

        public decimal BitcoinAmount { get; private set; }

        public Order(decimal fiatAmount, CurrencyCode currency, decimal exchangeRate, decimal bitcoinAmount)
        {
            Id = Guid.NewGuid();
            CreatedAt = DateTime.UtcNow;
            FiatAmount = fiatAmount;
            Currency = currency;
            ExchangeRate = exchangeRate;
            BitcoinAmount = bitcoinAmount;
        }
    }
}
