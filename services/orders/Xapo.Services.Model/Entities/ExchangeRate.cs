﻿using System;
using Xapo.Services.Model.Enums;

namespace Xapo.Services.Model.Entities
{
    public class ExchangeRate
    {
        public CurrencyCode Id { get; private set; }

        public DateTime CreatedAt { get; private set; }

        public decimal Rate { get; private set; }

        public ExchangeRate(CurrencyCode code, DateTime createdAt, decimal rate)
        {
            Id = code;
            CreatedAt = createdAt;
            Rate = rate;
        }

        public void Update(DateTime createdAt, decimal rate)
        {
            CreatedAt = createdAt;
            Rate = rate;
        }
    }
}
