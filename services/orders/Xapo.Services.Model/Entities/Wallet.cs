﻿namespace Xapo.Services.Model.Entities
{
    public class Wallet
    {
        public const decimal MaxValue = 100;

        public string Id { get; private set; }

        public int Version { get; private set; }

        public decimal AvailableAmount { get; private set; }

        public Wallet(string id)
        {
            Id = id;
            Version = 0;
            AvailableAmount = MaxValue;
        }

        public void DecreaseAvailableAmount(decimal amount)
        {
            AvailableAmount -= amount;
            Version++;
        }
    }
}
