﻿namespace Xapo.Services.Model.Enums
{
    public enum CurrencyCode
    {
        Eur = 0,
        Gbp,
        Usd
    }
}