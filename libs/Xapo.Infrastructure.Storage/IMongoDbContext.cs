﻿using MongoDB.Driver;

namespace Xapo.Infrastructure.Storage
{
    public interface IMongoDbContext : IUnitOfWork
    {
        IMongoClient Client { get; }

        bool InTransaction { get; }

        IMongoCollection<T> GetOrCreateCollection<T>();

        IMongoCollection<T> GetOrCreateCollection<T>(string name);
    }
}
