﻿using MongoDB.Driver;
using System.Threading.Tasks;

namespace Xapo.Infrastructure.Storage
{
    public interface IUnitOfWork
    {
        IClientSessionHandle JoinTransaction();

        Task<IClientSessionHandle> CreateTransaction();

        Task Commit();

        Task Rollback();
    }
}
