﻿namespace Xapo.Infrastructure.Configuration
{
    public class SerilogOptions
    {
        public bool ConsoleEnabled { get; set; }

        public string Level { get; set; }

        public bool ShowOnlyAudit { get; set; }
    }
}
