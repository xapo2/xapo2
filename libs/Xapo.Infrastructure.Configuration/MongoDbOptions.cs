﻿using System.Collections.Generic;

namespace Xapo.Infrastructure.Configuration
{
    public class MongoDbOptions
    {
        public IEnumerable<MongoServer> Servers { get; set; }

        public string DatabaseName { get; set; }

        public string AuthenticationDatabase { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public int Timeout { get; set; }

        public string ReplicaSet { get; set; }
    }

    public class MongoServer
    {
        public string Host { get; set; }

        public int Port { get; set; }
    }
}
