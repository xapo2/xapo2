﻿using Microsoft.Extensions.Configuration;

namespace Xapo.Infrastructure.Configuration
{
    public static class ConfigurationExtensions
    {
        public static TModel GetOptions<TModel>(this IConfiguration configuration)
            where TModel : new() =>
            GetOptions<TModel>(configuration, typeof(TModel).Name);

        public static TModel GetOptions<TModel>(this IConfiguration configuration, string sectionName)
            where TModel : new() =>
            configuration.GetSection(sectionName).Get<TModel>() ?? new TModel();
    }
}