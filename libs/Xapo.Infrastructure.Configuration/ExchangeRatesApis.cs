﻿using System;
using System.Collections.Generic;

namespace Xapo.Infrastructure.Configuration
{
    public class ExchangeRatesApis
    {
        public Dictionary<string, ApiOptions> ApiOptions { get; set; }
    }

    public class ApiOptions
    {
        public Uri Address { get; set; }

        public bool IsEnabled { get; set; }
    }
}
