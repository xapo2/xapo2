﻿using MongoDB.Driver;
using System;
using System.Linq;

namespace Xapo.Infrastructure.Configuration
{
    public static class MongoDbOptionsExtensions
    {
        public static MongoClientSettings ToMongoClientSettings(this MongoDbOptions mongoDbSettings)
        {
            var credential = !string.IsNullOrEmpty(mongoDbSettings.AuthenticationDatabase)
                ? MongoCredential.CreateCredential(mongoDbSettings.AuthenticationDatabase, mongoDbSettings.Username,
                    mongoDbSettings.Password)
                : null;

            return new MongoClientSettings
            {
                Servers = mongoDbSettings.Servers.Select(s => new MongoServerAddress(s.Host, s.Port)),
                ConnectionMode = !string.IsNullOrEmpty(mongoDbSettings.ReplicaSet) ?
                    ConnectionMode.ReplicaSet :
                    ConnectionMode.Direct,
                ReplicaSetName = !string.IsNullOrEmpty(mongoDbSettings.ReplicaSet) ? mongoDbSettings.ReplicaSet : null,
                Credential = credential,
                ConnectTimeout = TimeSpan.FromMilliseconds(mongoDbSettings.Timeout),
                ServerSelectionTimeout = TimeSpan.FromMilliseconds(mongoDbSettings.Timeout)
            };
        }
    }
}
