﻿using System;

namespace Xapo.Infrastructure.Remoting
{
    public static class Extensions
    {
        private static readonly Random Jitterer = new Random();

        public static TimeSpan AddJitter(
            this TimeSpan timeSpan,
            int minMilliseconds = 500,
            int maxMilliseconds = 2000) =>
            timeSpan.Add(TimeSpan.FromMilliseconds(Jitterer.Next(minMilliseconds, maxMilliseconds)));
    }
}
