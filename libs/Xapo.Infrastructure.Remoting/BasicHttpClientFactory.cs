﻿using Flurl.Http.Configuration;
using Microsoft.Extensions.Http;
using Microsoft.Extensions.Logging;
using Polly;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Xapo.Infrastructure.Remoting
{
    public class BasicHttpClientFactory : DefaultHttpClientFactory
    {
        private readonly ILogger<BasicHttpClientFactory> _logger;
        private readonly IAsyncPolicy<HttpResponseMessage> _httpPolicy;

        public BasicHttpClientFactory(ILogger<BasicHttpClientFactory> logger)
        {
            var generalRetryPolicy = Policy
                .Handle<HttpRequestException>()
                .OrResult<HttpResponseMessage>(InternalServerErrorPredicate)
                .WaitAndRetryAsync(3, retry => ExponentialBackoffProvider(retry).AddJitter(), LogRetry);
            var circuitBreakerPolicy = Policy
                .Handle<HttpRequestException>()
                .CircuitBreakerAsync(5, TimeSpan.FromSeconds(60), LogBreak, null);

            _httpPolicy = circuitBreakerPolicy.WrapAsync(generalRetryPolicy);
            _logger = logger;
        }

        public override HttpMessageHandler CreateMessageHandler() =>
            new PolicyHttpMessageHandler(_httpPolicy) { InnerHandler = base.CreateMessageHandler() };

        private bool InternalServerErrorPredicate(HttpResponseMessage msg) =>
            msg.StatusCode == HttpStatusCode.InternalServerError;

        private TimeSpan ExponentialBackoffProvider(int retry) =>
            TimeSpan.FromSeconds(Math.Pow(2, retry));

        private Task LogRetry(DelegateResult<HttpResponseMessage> delegateResult, TimeSpan time, int retry, Context ctx)
        {
            if (delegateResult.Exception != null)
            {
                _logger.LogWarning(
                    "Attempt {retry} retry after unsuccessful remote service call ({message}).",
                    retry,
                    delegateResult.Exception.InnerException?.Message ?? delegateResult.Exception.Message);
            }
            else
            {
                _logger.LogWarning(
                    "Attempt {retry} retry after unsuccessful remote service call ({code} {reason}).",
                    retry,
                    delegateResult.Result.StatusCode,
                    delegateResult.Result.ReasonPhrase);
            }

            return Task.CompletedTask;
        }

        private void LogBreak(Exception exception, TimeSpan timeSpan) =>
            _logger.LogError(exception, "Further calls to will be failed by circuit breaker for {timeSpan}.", timeSpan);
    }
}
