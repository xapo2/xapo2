﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;
using Xapo.Infrastructure.Configuration;

namespace Xapo.Infrastructure.Api
{
    public static class Extensions
    {
        public static IApplicationBuilder UseSwaggerDocs(this IApplicationBuilder builder)
        {
            var options = builder.ApplicationServices.GetRequiredService<IOptions<SwaggerOptions>>().Value;

            if (!options.Enabled)
                return builder;

            var routePrefix = string.IsNullOrWhiteSpace(options.RoutePrefix) ? "swagger" : options.RoutePrefix;

            builder
                .UseStaticFiles()
                .UseSwagger(c => c.RouteTemplate = routePrefix + "/{documentName}/swagger.json");

            return builder.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint($"/{routePrefix}/{options.Name}/swagger.json", options.Title);

                    c.RoutePrefix = routePrefix;
                });
        }

        public static IServiceCollection AddSwaggerDocs(
            this IServiceCollection services,
            IConfiguration configuration,
            string appName)
        {
            var options = configuration.GetOptions<SwaggerOptions>(nameof(SwaggerOptions));

            services.Configure<SwaggerOptions>(configuration.GetSection(nameof(SwaggerOptions)));

            return options.Enabled ? AddSwaggerGen(services, options, appName) : services;
        }

        private static IServiceCollection AddSwaggerGen(
            IServiceCollection services,
            SwaggerOptions options,
            string appName = null) =>
            services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc(options.Name, new OpenApiInfo { Title = options.Title, Version = options.Version });
                    c.CustomSchemaIds(x => x.FullName);

                    if (options.IncludeSecurity)
                    {
                        c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                        {
                            Description =
                                "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                            Name = "Authorization",
                            In = ParameterLocation.Header,
                            Type = SecuritySchemeType.ApiKey
                        });
                    }

                    if (options.UseXmlDoc)
                    {
                        var xmlFile = appName == null
                            ? $"{Assembly.GetEntryAssembly().GetName().Name}.xml"
                            : $"{appName}.xml";
                        var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                        c.IncludeXmlComments(xmlPath);
                    }
                })
                .AddSwaggerGenNewtonsoftSupport();
    }
}
