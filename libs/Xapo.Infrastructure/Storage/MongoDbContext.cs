﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xapo.Infrastructure.Configuration;

namespace Xapo.Infrastructure.Storage
{
    public class MongoDbContext : IMongoDbContext
    {
        private readonly IMongoDatabase _db;
        private readonly int _clusterSize;

        private IClientSessionHandle _sessionHandle;

        public IMongoClient Client { get; }

        public bool InTransaction => _sessionHandle?.IsInTransaction == true;

        public MongoDbContext(IOptions<MongoDbOptions> options)
        {
            var mongoSettings = options.Value;
            var clientSettings = mongoSettings.ToMongoClientSettings();

            Client = new MongoClient(clientSettings);
            _clusterSize = mongoSettings.Servers.Count();
            _db = Client.GetDatabase(mongoSettings.DatabaseName);
        }

        public IClientSessionHandle JoinTransaction()
        {
            if (_sessionHandle == null || !_sessionHandle.IsInTransaction)
                throw new Exception("There is no ongoing transaction to join to.");

            return _sessionHandle;
        }

        public async Task<IClientSessionHandle> CreateTransaction()
        {
            _sessionHandle ??= await _db.Client.StartSessionAsync(new ClientSessionOptions
            {
                CausalConsistency = true,
                DefaultTransactionOptions = new TransactionOptions(
                    writeConcern: new WriteConcern(
                        w: new WriteConcern.WCount(_clusterSize)))
            });

            if (!_sessionHandle.IsInTransaction)
                _sessionHandle.StartTransaction();

            return _sessionHandle;
        }

        public async Task Commit()
        {
            await _sessionHandle.CommitTransactionAsync();

            _sessionHandle = null;
        }

        public async Task Rollback()
        {
            if (_sessionHandle?.IsInTransaction != true)
                return;

            await _sessionHandle?.AbortTransactionAsync();

            _sessionHandle = null;
        }

        public IMongoCollection<T> GetOrCreateCollection<T>() =>
            GetOrCreateCollection<T>(typeof(T).Name);

        public IMongoCollection<T> GetOrCreateCollection<T>(string name)
        {
            var filter = new BsonDocument("name", name);
            var options = new ListCollectionNamesOptions { Filter = filter };

            if (!_db.ListCollectionNames(options).Any())
                _db.CreateCollection(name);

            // this won't create collection if there's no database,
            // that's why this method is like it is
            return _db.GetCollection<T>(name);
        }
    }
}
