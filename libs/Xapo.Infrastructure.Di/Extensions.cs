﻿using Autofac;
using System.Reflection;

namespace Xapo.Infrastructure.Di
{
    public static class ContainerBuilderExtensions
    {
        public static void RegisterByNamespacePart(this ContainerBuilder builder, Assembly asm, string namespacePart) =>
            builder
                .RegisterAssemblyTypes(asm)
                .Where(t => !string.IsNullOrEmpty(t.Namespace) && t.Namespace.Contains(namespacePart))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope()
                .PreserveExistingDefaults();
    }
}
