﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using System;
using Xapo.Infrastructure.Configuration;

namespace Xapo.Infrastructure.Logging
{
    public static class Extensions
    {
        public static IWebHostBuilder UseLogging(this IWebHostBuilder webHostBuilder, LoggingLevelSwitch logSwitch, string applicationName = null)
            => webHostBuilder.UseSerilog((context, loggerConfiguration) =>
            {
                var appOptions = context.Configuration.GetOptions<AppOptions>();
                var serilogOptions = context.Configuration.GetOptions<SerilogOptions>(nameof(SerilogOptions));

                if (!Enum.TryParse<LogEventLevel>(serilogOptions.Level, true, out var level)) 
                    level = LogEventLevel.Warning;

                logSwitch.MinimumLevel = level;
                applicationName = string.IsNullOrWhiteSpace(applicationName) 
                    ? appOptions.Name 
                    : applicationName;

                loggerConfiguration
                    .ReadFrom.Configuration(context.Configuration)
                    .Enrich.FromLogContext()
                    .MinimumLevel.ControlledBy(logSwitch)
                    .Enrich.WithProperty("Environment", context.HostingEnvironment.EnvironmentName)
                    .Enrich.WithProperty("ApplicationName", applicationName);

                if (serilogOptions.ShowOnlyAudit && level == LogEventLevel.Information)
                    loggerConfiguration.Filter.ByExcluding(x =>
                        x.Level == LogEventLevel.Information && !x.Properties.ContainsKey("Priority"));

                if (serilogOptions.ConsoleEnabled)
                    loggerConfiguration.WriteTo.Console();
            });

        public static IWebHostBuilder UseAppConfig(
            this IWebHostBuilder webHostBuilder,
            Type startupType,
            params string[] otherConfigs) =>
            UseAppConfig(webHostBuilder, startupType.Namespace!.Split(".")[^1], otherConfigs);

        public static IWebHostBuilder UseAppConfig(
            this IWebHostBuilder webHostBuilder,
            string serviceName,
            params string[] otherConfigs) =>
            webHostBuilder.ConfigureAppConfiguration((hostingContext, config) =>
            {
                config.SetBasePath(Environment.CurrentDirectory);
                config.AddJsonFile($"appsettings.{serviceName}.json", optional: false);
                config.AddJsonFile(
                    $"appsettings.{serviceName}.{hostingContext.HostingEnvironment.EnvironmentName}.json",
                    optional: true);
                config.AddJsonFile($"secret.{serviceName}.{hostingContext.HostingEnvironment.EnvironmentName}.json",
                    optional: true);

                foreach (var name in otherConfigs)
                {
                    config.AddJsonFile($"{name}.{serviceName}.json", optional: false);
                    config.AddJsonFile($"{name}.{serviceName}.{hostingContext.HostingEnvironment.EnvironmentName}.json",
                        optional: true);
                }

                config.AddEnvironmentVariables();
            });
    }
}
