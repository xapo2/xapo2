﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading.Tasks;
using Xapo.Infrastructure.Storage;
using Xapo.Services.Application.Dtos.Requests;
using Xapo.Services.Application.Dtos.Results.CreateBuyOrder;
using Xapo.Services.Application.Services;
using Xapo.Services.Application.Services.ExchangeRateStrategies;
using Xapo.Services.Infrastructure.Repository;
using Xapo.Services.Model.Entities;
using Xapo.Services.Model.Enums;
using Xunit;

namespace Xapo.Services.Application.Tests
{
    public class CreateBuyOrderTests
    {
        private readonly IUnitOfWork _unitOfWorkMock;
        private readonly Mock<IWalletRepository> _walletRepositoryMock;
        private readonly Mock<IOrderRepository> _orderRepositoryMock;
        private readonly Mock<IExchangeRateRepository> _exchangeRateRepositoryMock;
        private readonly Mock<IExchangeRateApiStrategy> _exchangeRateLoadingStrategy;
        private readonly ILogger<CreateBuyOrder> _logger;
        private readonly CreateBuyOrder _sut;

        public CreateBuyOrderTests()
        {
            _unitOfWorkMock = Mock.Of<IUnitOfWork>();
            _walletRepositoryMock = new Mock<IWalletRepository>();
            _orderRepositoryMock = new Mock<IOrderRepository>();
            _exchangeRateRepositoryMock = new Mock<IExchangeRateRepository>();
            _exchangeRateLoadingStrategy = new Mock<IExchangeRateApiStrategy>();
            _logger = Mock.Of<ILogger<CreateBuyOrder>>();
            _sut = new CreateBuyOrder(
                _unitOfWorkMock,
                _walletRepositoryMock.Object,
                _orderRepositoryMock.Object,
                _exchangeRateRepositoryMock.Object,
                new[] {_exchangeRateLoadingStrategy.Object},
                _logger);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task GivenBuyOrderCommand_WhenTheAmountIsZeroOrNegative_ThenCreateBuyOrderErrorResultIsReturned(decimal amount)
        {
            var result = await _sut.Execute(new CreateBuyOrderCommand
            {
                Amount = amount,
                CurrencyCode = CurrencyCode.Eur
            });

            result.Should().BeEquivalentTo(new CreateBuyOrderErrorResult(CreateBuyOrderErrorCode.AmountNegativeOrZero));
        }

        [Fact]
        public async Task GivenBuyOrderCommand_WhenTheDatabaseDoesntContainAnExchangeRate_ThenCreateBuyOrderErrorResultIsReturned()
        {
            var result = await _sut.Execute(new CreateBuyOrderCommand
            {
                Amount = 1,
                CurrencyCode = CurrencyCode.Eur
            });

            result.Should().BeEquivalentTo(new CreateBuyOrderErrorResult(CreateBuyOrderErrorCode.NoExchangeRateAvailable));
        }

        [Fact]
        public async Task GivenBuyOrderCommand_WhenTheExchangeRatesApiReturnsData_AndThereIsNoWalletPresent_ThenCreateBuyOrderErrorResultIsReturned()
        {
            _exchangeRateLoadingStrategy
                .Setup(s => s.GetRate(It.IsAny<CurrencyCode>()))
                .ReturnsAsync(new ExchangeRate(CurrencyCode.Eur, DateTime.UtcNow, 1));
            _exchangeRateLoadingStrategy
                .Setup(s => s.CanRun())
                .Returns(true);

            var result = await _sut.Execute(new CreateBuyOrderCommand
            {
                Amount = 1,
                CurrencyCode = CurrencyCode.Eur
            });

            result.Should()
                .BeEquivalentTo(new CreateBuyOrderErrorResult(CreateBuyOrderErrorCode.WalletVersionMismatch));
        }

        [Theory]
        [InlineData(10, 11)]
        [InlineData(0, 1)]
        public async Task GivenBuyOrderCommand_WhenTheWalletAndExchangeRatesArePresent_AndTheTransactionWouldExceedTheAvailableAmount_ThenCreateBuyOrderErrorResultIsReturned(decimal walletAmount, decimal fiatAmount)
        {
            var wallet = new Wallet(Infrastructure.Configuration.Constants.WalletId);

            _exchangeRateLoadingStrategy
                .Setup(s => s.GetRate(It.IsAny<CurrencyCode>()))
                .ReturnsAsync(new ExchangeRate(CurrencyCode.Eur, DateTime.UtcNow, 1));
            _exchangeRateLoadingStrategy
                .Setup(s => s.CanRun())
                .Returns(true);
            _walletRepositoryMock
                .Setup(x => x.Load(It.IsAny<int>()))
                .ReturnsAsync(wallet);

            wallet.SetPrivateProperty(w => w.AvailableAmount, walletAmount);

            var result = await _sut.Execute(new CreateBuyOrderCommand
            {
                Amount = fiatAmount,
                CurrencyCode = CurrencyCode.Eur
            });

            result.Should()
                .BeEquivalentTo(new CreateBuyOrderErrorResult(CreateBuyOrderErrorCode.AmountExceedsAvailableAmount));
        }

        [Fact]
        public async Task GivenBuyOrderCommand_WhenWalletUpdateThrowsAnException_ThenCreateBuyOrderErrorResultIsReturned()
        {
            var wallet = new Wallet(Infrastructure.Configuration.Constants.WalletId);

            _exchangeRateLoadingStrategy
                .Setup(s => s.GetRate(It.IsAny<CurrencyCode>()))
                .ReturnsAsync(new ExchangeRate(CurrencyCode.Eur, DateTime.UtcNow, 1));
            _exchangeRateLoadingStrategy
                .Setup(s => s.CanRun())
                .Returns(true);
            _walletRepositoryMock
                .Setup(x => x.Load(It.IsAny<int>()))
                .ReturnsAsync(wallet);
            _walletRepositoryMock
                .Setup(x => x.Update(It.IsAny<Wallet>()))
                .ThrowsAsync(new Exception("test"));

            var result = await _sut.Execute(new CreateBuyOrderCommand
            {
                Amount = 1,
                CurrencyCode = CurrencyCode.Eur
            });

            result.Should()
                .BeEquivalentTo(new CreateBuyOrderErrorResult(CreateBuyOrderErrorCode.WalletVersionMismatch));
        }

        [Fact]
        public async Task GivenBuyOrderCommand_WhenAllTheRequiredDataIsPresent_ThenAnOrderIsCreated_AndTheWalletAmountIsDecreasedAccordingly()
        {
            var wallet = new Wallet(Infrastructure.Configuration.Constants.WalletId);

            _exchangeRateLoadingStrategy
                .Setup(s => s.GetRate(It.IsAny<CurrencyCode>()))
                .ReturnsAsync(new ExchangeRate(CurrencyCode.Eur, DateTime.UtcNow, 1));
            _exchangeRateLoadingStrategy
                .Setup(s => s.CanRun())
                .Returns(true);
            _walletRepositoryMock
                .Setup(x => x.Load(It.IsAny<int>()))
                .ReturnsAsync(wallet);

            var result = await _sut.Execute(new CreateBuyOrderCommand
            {
                Amount = 10,
                CurrencyCode = CurrencyCode.Eur
            });

            result.GetType().Should().Be(typeof(CreateBuyOrderOkResult));
        }
    }
}
