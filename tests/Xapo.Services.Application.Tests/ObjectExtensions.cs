﻿using System;
using System.Linq.Expressions;

namespace Xapo.Services.Application.Tests
{
    public static class ObjectExtensions
    {
        public static void SetPrivateProperty<TInstance, TValue>(
            this TInstance instance,
            Expression<Func<TInstance, TValue>> selector,
            TValue value)
        {
            if (!(selector.Body is MemberExpression mBody))
            {
                var uBody = (UnaryExpression)selector.Body;
                mBody = uBody.Operand as MemberExpression;
            }

            instance.GetType().GetProperty(mBody.Member.Name).SetValue(instance, value);
        }
    }
}
