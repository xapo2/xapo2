#!/bin/bash

rm -f docker-compose.merged.yml

should_run_apps=${1:-"--no-apps"}
infra_ymls=`ls **/docker-compose.*.yml`
svc_ymls=`ls ../services/**/**/docker-compose.yml;`
compose_string=""

for file in $infra_ymls
do
    compose_string="${compose_string} -f ${file}"
done
if [ $should_run_apps == "--with-apps" ]; then
    for file in $svc_ymls
    do
        compose_string="${compose_string} -f ${file}"
    done
fi

docker-compose $compose_string --project-directory ./ config | iconv -f WINDOWS-1250 -t UTF-8 > docker-compose.merged.yml
docker-compose  --compatibility -f docker-compose.merged.yml up -d --build --force-recreate
